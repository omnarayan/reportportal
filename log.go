package reportportal

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/go-resty/resty/v2"
)

// LogService handles log for the report portal instance / API.
type LogService struct {
	client *resty.Client
}

// FileName file name
type FileName struct {
	Name string `json:"name,omitempty" structs:"name,omitempty"`
}

//Log structure
type Log struct {
	ID       string    `json:"id,omitempty" structs:"id,omitempty"`
	UUID     string    `json:"uuid,omitempty" structs:"uuid,omitempty"`
	LaunchID string    `json:"launchUuid,omitempty" structs:"launchUuid,omitempty"`
	Name     string    `json:"name,omitempty" structs:"name,omitempty"`
	FileName FileName  `json:"file,omitempty" structs:"file,omitempty"`
	ItemUuID string    `json:"itemUuid,omitempty" structs:"itemUuid,omitempty"`
	Message  string    `json:"message,omitempty" structs:"message,omitempty"`
	Level    string    `json:"level,omitempty" structs:"level,omitempty"`
	LogTime  time.Time `json:"logTime,omitempty" structs:"logTime,omitempty"`
	LTime    time.Time `json:"time,omitempty" structs:"time,omitempty"`
}

// Create create log
func (l *LogService) Create(logType string, log *Log) error {
	log.LTime = time.Now()
	log.LogTime = time.Now()

	logs := []*Log{log}
	requestByte, _ := json.Marshal(logs)

	resp, err := l.client.R().
		// SetFileReader(logType, log.FileName.Name, bytes.NewReader(data)).
		SetMultipartField("json_request_part", "", "application/json", strings.NewReader(string(requestByte))).
		SetBody(log).
		Post(strings.TrimSpace(logURL))
	if err == nil {
		if resp.StatusCode() != 201 {
			return fmt.Errorf("Unable to attache log status code - '%d', responce %s", resp.StatusCode(), string(resp.Body()))
		}
	}
	err = json.Unmarshal(resp.Body(), &log)
	return err
}
