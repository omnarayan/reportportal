package reportportal

import (
	"fmt"
	"os"
	"time"

	"github.com/go-resty/resty/v2"
)

var (
	// Client resty
	client *resty.Client
	// BaseURL base url
	BaseURL = ""
	// project list
	projectListURL = ""
	// start launch
	launchStartURL = ""
	// launch list
	launchFinishURL = ""
	//test result start
	testStartURL = ""
	//test finish start
	testFinishURL = ""
	logURL        = ""
)

// A Client manages communication with the Report Portal API.
type Client struct {
	client  *resty.Client
	baseURL string
	token   string
	Project *ProjectService
	Launch  *LanuchService
	Item    *ItemService
	Log     *LogService
}

// NewClient returns a new Report Portal API client.
func NewClient(baseURL, token, projectName string) (*Client, error) {
	fmt.Println("baseURL ", baseURL)
	var err error
	c := &Client{
		client:  resty.New(),
		baseURL: baseURL,
		token:   token,
	}
	if os.Getenv("DEBUG") == "YES" {
		c.client.SetDebug(true)
	}
	// Set client timeout as per your need
	c.client.SetTimeout(30 * time.Second)
	c.client.SetHostURL(baseURL)
	c.client.SetHeader("Accept", "application/json")
	c.client.SetHeader("Content-Type", "application/json")
	// Bearer Auth Token for all request
	c.client.SetAuthToken(token)

	// Enabling Content length value for all request
	c.client.SetContentLength(true)

	projectListURL = fmt.Sprintf("/%s/list", "project")
	launchStartURL = fmt.Sprintf("/%s/launch", projectName)
	launchFinishURL = fmt.Sprintf("/%s/launch", projectName)
	testStartURL = fmt.Sprintf("/%s/item", projectName)
	testFinishURL = fmt.Sprintf("/%s/item", projectName)
	logURL = fmt.Sprintf("/%s/log", projectName)

	c.Project = &ProjectService{client: c.client}
	c.Launch = &LanuchService{client: c.client}
	c.Item = &ItemService{client: c.client}
	c.Log = &LogService{client: c.client}
	return c, err
}
