package reportportal

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/go-resty/resty/v2"
)

// ItemService handles Itemm for the report portal instance / API.
type ItemService struct {
	client *resty.Client
}

//Item structure
type Item struct {
	ID          string      `json:"id,omitempty" structs:"id,omitempty"`
	UUID        string      `json:"uuid,omitempty" structs:"uuid,omitempty"`
	LaunchID    string      `json:"launchUuid,omitempty" structs:"launchUuid,omitempty"`
	Name        string      `json:"name,omitempty" structs:"name,omitempty"`
	Description string      `json:"description,omitempty" structs:"description,omitempty"`
	Link        string      `json:"link,omitempty" structs:"link,omitempty"`
	Type        string      `json:"type,omitempty" structs:"type,omitempty"`
	TestCaseID  string      `json:"testCaseId,omitempty" structs:"testCaseId,omitempty"`
	Retry       bool        `json:"retry,omitempty" structs:"retry,omitempty"`
	Rerun       bool        `json:"rerun,omitempty" structs:"rerun,omitempty"`
	RerunOf     string      `json:"rerunOf,omitempty" structs:"rerunOf,omitempty"`
	Status      string      `json:"status,omitempty" structs:"status,omitempty"`
	StartTime   time.Time   `json:"startTime,omitempty" structs:"startTime,omitempty"`
	EndTime     time.Time   `json:"endTime,omitempty" structs:"endTime,omitempty"`
	Attributes  []Attribute `json:"attributes,omitempty" structs:"attributes,omitempty"`
	Parameters  []Attribute `json:"parameters,omitempty" structs:"parameters,omitempty"`
	// Client
}

// Start start item
func (i *ItemService) Start(parentID string, item *Item) error {
	url := testStartURL
	if testStartURL != "" {
		url = testStartURL + "/" + parentID
	}
	resp, err := i.client.R().
		SetBody(item).
		Post(strings.TrimSpace(url))
	if err == nil {
		if resp.StatusCode() != 201 {
			return fmt.Errorf("Unable to start item status code - '%d', responce %s", resp.StatusCode(), string(resp.Body()))
		}
	}
	fmt.Println("resp.Body() ", resp.String())
	err = json.Unmarshal(resp.Body(), &item)
	return err
}

// Finish Finish launch
func (i *ItemService) Finish(itemID string, item *Item) error {
	url := fmt.Sprintf("%s/%s", testFinishURL, itemID)
	item.Status = strings.ToUpper(item.Status)
	switch item.Status {
	case "PASS":
		item.Status = "PASSED"
	case "FAIL":
		item.Status = "FAILED"
	case "ERROR":
		item.Status = "FAILED"
	default:
		item.Status = "SKIPPED"
	}
	resp, err := i.client.R().
		SetBody(item).
		Put(strings.TrimSpace(url))
	if err == nil {
		if resp.StatusCode() != 200 {
			return fmt.Errorf("Unable to start item status code - '%d', responce %s", resp.StatusCode(), string(resp.Body()))
		}
	}
	fmt.Println("resp.Body() ", resp.String())
	err = json.Unmarshal(resp.Body(), &item)
	return err
}
func Upload(client *http.Client, url string, values map[string]io.Reader) (err error) {
	// Prepare a form that you will submit to that URL.
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	for key, r := range values {
		var fw io.Writer
		if x, ok := r.(io.Closer); ok {
			defer x.Close()
		}
		// Add an image file
		if x, ok := r.(*os.File); ok {
			if fw, err = w.CreateFormFile(key, x.Name()); err != nil {
				return
			}
		} else {
			// Add other fields
			if fw, err = w.CreateFormField(key); err != nil {
				return
			}
		}
		if _, err = io.Copy(fw, r); err != nil {
			return err
		}

	}
	// Don't forget to close the multipart writer.
	// If you don't close it, your request will be missing the terminating boundary.
	w.Close()

	// Now that you have a form, you can submit it to your handler.
	req, err := http.NewRequest("POST", url, &b)
	if err != nil {
		return
	}
	// Don't forget to set the content type, this will contain the boundary.
	req.Header.Set("Content-Type", w.FormDataContentType())

	// Submit the request
	res, err := client.Do(req)
	if err != nil {
		return
	}

	// Check the response
	if res.StatusCode != http.StatusOK {
		err = fmt.Errorf("bad status: %s", res.Status)
	}
	return
}
