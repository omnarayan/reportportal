package reportportal

import (
	"encoding/json"
	"fmt"

	"github.com/go-resty/resty/v2"
)

// ProjectService handles Project for the report portal instance / API.
type ProjectService struct {
	client *resty.Client
}

// Project structure
type Project struct {
	ID          int    `json:"id" structs:"id"`
	ProjectName string `json:"projectName" structs:"projectName"`
	Data        string `json:"data" structs:"data"`
}

type projectContent struct {
	// Page     map[string]interface{} `json:"page" structs:"page"`
	Projects []Project `json:"content" structs:"content"`
}

// GetAll all Project
func (p *ProjectService) GetAll() ([]Project, error) {
	fmt.Println("projectListURL ", projectListURL)
	content := projectContent{}
	resp, err := p.client.R().
		Get(projectListURL)
	if err == nil {
		if resp.StatusCode() != 200 {
			return content.Projects, fmt.Errorf("Unable to get status code - '%d', responce %s", resp.StatusCode(), resp.String())
		}
	}
	err = json.Unmarshal(resp.Body(), &content)
	return content.Projects, err
}
