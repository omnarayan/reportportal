package reportportal

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/go-resty/resty/v2"
)

// LanuchService handles Launch for the report portal instance / API.
type LanuchService struct {
	client *resty.Client
}

// Attribute structure
type Attribute struct {
	Key   string `json:"key" structs:"key"`
	Value string `json:"value" structs:"value"`
}

//Launch structure
type Launch struct {
	ID          string      `json:"id,omitempty" structs:"id,omitempty"`
	UUID        string      `json:"uuid,omitempty" structs:"uuid,omitempty"`
	Name        string      `json:"name,omitempty" structs:"name,omitempty"`
	Description string      `json:"description,omitempty" structs:"description,omitempty"`
	ProjectID   string      `json:"projectID,omitempty" structs:"projectID,omitempty"`
	BuildID     string      `json:"buildID,omitempty" structs:"buildID,omitempty"`
	User        string      `json:"user,omitempty" structs:"user,omitempty"`
	Link        string      `json:"link,omitempty" structs:"link,omitempty"`
	Mode        string      `json:"mode,omitempty" structs:"mode,omitempty"`
	HasRetries  bool        `json:"hasRetries,omitempty" structs:"hasRetries,omitempty"`
	Rerun       bool        `json:"rerun,omitempty" structs:"rerun,omitempty"`
	RerunOf     string      `json:"rerunOf,omitempty" structs:"rerunOf,omitempty"`
	StartTime   time.Time   `json:"startTime,omitempty" structs:"startTime,omitempty"`
	EndTime     time.Time   `json:"endTime,omitempty" structs:"endTime,omitempty"`
	Attributes  []Attribute `json:"attributes,omitempty" structs:"attributes,omitempty"`
	// Client
}

// Start start launch
func (l *LanuchService) Start(launch *Launch) error {
	resp, err := l.client.R().
		SetBody(launch).
		Post(strings.TrimSpace(launchStartURL))
	if err == nil {
		if resp.StatusCode() != 201 {
			return fmt.Errorf("Unable to start launch status code - '%d', responce %s", resp.StatusCode(), string(resp.Body()))
		}
	}
	fmt.Println("resp.Body() ", resp.String())
	err = json.Unmarshal(resp.Body(), &launch)
	return err
}

// Finish Finish launch
func (l *LanuchService) Finish(launchID string) error {
	url := fmt.Sprintf("%s/%s/finish", launchFinishURL, launchID)
	launch := Launch{
		EndTime: time.Now(),
	}
	resp, err := l.client.R().
		SetBody(launch).
		Put(strings.TrimSpace(url))
	if err == nil {
		if resp.StatusCode() != 200 {
			return fmt.Errorf("Unable to start launch status code - '%d', responce %s", resp.StatusCode(), string(resp.Body()))
		}
	}
	fmt.Println("resp.Body() ", resp.String())
	err = json.Unmarshal(resp.Body(), &launch)
	return err
}
